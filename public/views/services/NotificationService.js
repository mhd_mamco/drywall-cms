'use strict';

angular.module('ProjectName')
	.factory('NotificationService', function () {
		return {
			alert: function (message) {
				if (navigator.notification) {
					navigator.notification.alert(message);
				}
				else{
					alert(message);
				}
			},
			confirm: function(message, callback, title, buttonLabels){
				if(navigator.notification){
					navigator.notification.confirm(message, callback, title, buttonLabels);
				}
				else{
					alert(message);
				}
			},
			load: function() {
				try {
					window.wizSpinner.show({
						customSpinner : false
					});
				}catch(e){
					console.debug("Loading... ");
				}
			},
			stopLoad: function() {
				try {
					window.wizSpinner.hide();
				}catch(e){
					console.debug("Stop loading");
				}
			}
		};
	}).factory('pushNotificationService', ['companionPushService', 'AccountService', '$q', 'notificationService', '$rootScope', function(companionPushService, AccountService, $q, notificationService, $rootScope){
		var pushNotification,
			registrationPromise = $q.defer(),
			localEvents = {
				LOGGED_IN: 'pushNotificationService:LOGGED_IN',
				LOGGED_OUT: 'pushNotificationService:LOGGED_OUT'
			};

		//Android
		window.onNotificationGCM = function(e){
			console.log(angular.toJson(e));
			console.log(angular.toJson(e.payload));
			switch( e.event )
			{
				case 'registered':
				if ( e.regid.length > 0 )
				{
					$rootScope.$broadcast(localEvents.LOGGED_IN);
					companionPushService.register(AccountService.token(), e.regid, 'gcm').then(function(){
						registrationPromise.resolve(e.regid);
					});
				}else{
					registrationPromise.reject('no token');
				}
				break;

				case 'message':
					// if this flag is set, this notification happened while we were in the foreground.
					// you might want to play a sound to get the user's attention, throw up a dialog, etc.
					var payload = e.payload,
							where;
						if(payload.program_id){
							where = 'programmes/' + payload.program_id + '/' + payload.program_key;
						}else if(payload.event_id){
							where = 'events/' + payload.si_traffic_key;
						}else if(payload.service_id){
							where = 'services/' + payload.service_id;
						}else{
							where = payload.path;
						}

					if (e.foreground)
					{
						notificationService.confirm(payload.message, function(e){ if(e==1){ $rootScope.goTo(where)} }, payload.title, 'Go,Cancel')
					}
					else
					{   // otherwise we were launched because the user touched a notification in the notification tray.
						$rootScope.goTo(where);
					}
				break;

				case 'error':
					notificationService.alert(e.msg);
				break;

				default:
					notificationService.alert('Don\'t know what happened');
				break;
			}

		}

		//iOS
		function onNotificationAPN(event) {
			if (event.alert) {
				notificationService.alert("alert");
				notificationService.alert(event.alert);
			}

			if (event.sound) {
				var snd = new Media(event.sound);
				snd.play();
			}

			if (event.badge) {
				notificationService.alert("badge");
				pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
			}
		}
		function tokenHandler (result) {
			// Send token to server
			notificationService.alert('device token = '+result);
		}
		function errorHandler (error) {
			console.log(error);
		}

		function init(){
			pushNotification = window.plugins.pushNotification;

			if (device.platform == 'android' || device.platform == 'Android') {
				pushNotification.register(function(a){
				}, function(a){
					registrationPromise.reject(a);
					console.log(a);
				},{"senderID":"199544139916","ecb":"onNotificationGCM"});
			} else {
				pushNotification.register(tokenHandler, errorHandler, {"badge":"true","sound":"true","alert":"true","ecb":"onNotificationAPN"});
			}
			return registrationPromise.promise;
		}

		function unregister(fn){
			if (device.platform == 'android' || device.platform == 'Android') {
				if(pushNotification){
					pushNotification.unregister(fn);
					companionPushService.unregister(AccountService.token(), 'gcm');
					$rootScope.$broadcast(localEvents.LOGGED_OUT);
				}else{
					init();
				}
			}else{
				if(pushNotification){
					pushNotification.unregister(fn);
					companionPushService.unregister(AccountService.token(), 'apn');
					$rootScope.$broadcast(localEvents.LOGGED_OUT);
				}else{
					init();
				}
			}
		}

		return{
			Events: localEvents,
			init: init,
			unregister: unregister
		}
	}]);