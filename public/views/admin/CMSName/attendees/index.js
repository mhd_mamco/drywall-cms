angular.module('ProjectName').controller('AttendeesCtrl', ['$scope', 'attendeesService', 'NotificationService',
	function($scope, attendeesService, NotificationService) {
		// Page will be NaN if we don't set it to 1 at first
		$scope.filters = {
			page: 1
		};

		function loadData(filters) {
			attendeesService.load(filters).then(function(response) {
				// Make sure the current filters correspond to this particular load
				if (filters == $scope.filters) {
					$scope.results = response.data;
					$scope.pages = response.pages;
					$scope.items = response.items;
				}
			}, function(err) {
				NotificationService.alert('There was an error loading attendee');
			});
		}
		var errMsg = 'you have the following problem(s): ';

		function resetFields() {
			$scope.userName = '';
			$scope.subscribeNews = '';
			$scope.email = '';
			$scope.password = '';
			$scope.provider = '';
			$scope.phoneNumbe = '';
		}

		$scope.$watch('filters', function(nv, ov) {
			loadData(nv);
		}, true);

		$scope.addNew = function() {
			console.log($scope);
			var attendee = {
				// firstName: $scope.firstName,
				userName: $scope.userName,
				email: $scope.email,
				password: $scope.password,
				provider: $scope.provider,
				subscribeNews: $scope.subscribeNews,
				loyaltyPoints: $scope.loyaltyPoints
			};
			attendeesService.create(attendee).then(_.bind(loadData, null, $scope.filters));
			resetFields();
		}

		$scope.nextPage = function() {
			if ($scope.pages.hasNext) {
				$scope.filters.page += 1;
			}

		}
		$scope.previousPage = function() {
			if ($scope.pages.hasPrev) {
				$scope.filters.page -= 1;
			}
		}

		function closeAllResults() {
			_.each($scope.results, function(result) {
				result.details = null;
			})
		}

		$scope.close = function() {
			closeAllResults();
		}

		$scope.editAttendee = function(index) {
			closeAllResults();
			var that = this;

			attendeesService.get(this.attendee._id).then(function(data) {
				that.attendee.details = data;
			});
		}


		$scope.saveAttendee = function(index) {

			attendeesService.update(this.attendee.details).then(function(data) {
				if (data.success) {
					loadData($scope.filters)
				} else if (data.errors) {
					NotificationService.alert(errMsg + data.errors.join(',\n'));
				}
				closeAllResults();

			});
		}

		$scope.deleteAttendee = function(index) {
			var result = confirm('you are about to remove ' + this.attendee.name + '\nconfirm');
			if (result) {
				attendeesService.delete(this.attendee._id).then(function(data) {
					if (data.success) {
						loadData($scope.filters);
					} else if (data.errors) {
						NotificationService.alert(errMsg + data.errors.join(',\n'));
					}
				});
			}
		}
	}
]);