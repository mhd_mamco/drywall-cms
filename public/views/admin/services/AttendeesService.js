angular.module('ProjectName').factory('attendeesService', ['ResourcesService', function(ResourcesService) {
	var Urls = {
		LOAD: '/admin/attendees/',
		CREATE: '/admin/attendees/',
		GET: _.template( '/admin/attendees/<%= id %>' ),
		UPDATE: _.template( '/admin/attendees/<%= id %>' ),
		DELETE: _.template( '/admin/attendees/<%= id %>' )
	};
	return ResourcesService(Urls); 
}]);