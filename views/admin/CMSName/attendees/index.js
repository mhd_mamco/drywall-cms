exports.find = function(req, res, next) {
	// Normal get displays the page
	if (!req.xhr) {
		res.render('admin/CMSName/attendees/index');
	} else {
		//defaults
		// req.query.title = req.query.title ? req.query.title : '';
		req.query.userName = req.query.userName ? req.query.userName : '';
		req.query.limit = req.query.limit ? parseInt(req.query.limit) : 20;
		req.query.page = req.query.page ? parseInt(req.query.page) : 1;
		req.query.sort = req.query.sort ? req.query.sort : '_id';

		//filters
		var filters = {};
		if (req.query.userName) {
			filters.userName = new RegExp('^.*?' + req.query.userName + '.*$', 'i');
		}		
		//get results
		req.app.db.models.Attendees.pagedFind({
			filters: filters,
			keys: 'userName email provider loyaltyPoints subscribeNews',
			limit: req.query.limit,
			page: req.query.page,
			sort: req.query.sort
		}, function(err, results) {
			if (err) {
				return next(err);
			}
			res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
			results.filters = req.query;
			res.send(results);
		});
	}
};


exports.read = function(req, res, next) {
	req.app.db.models.Attendees.findById(req.params.id).exec(function(err, attendee) {
		if (err) return next(err);
		res.send(attendee);
	});
};



exports.create = function(req, res, next) {
	var workflow = new req.app.utility.workflow(req, res);

	workflow.on('validate', function() {
		if (!req.user.roles.admin.isMemberOf('root')) {
			workflow.outcome.errors.push('You may not create Attendee.');
			return workflow.emit('response');
		}

		if (!req.body.email) {
			workflow.outcome.errors.push('An email is required.');
			return workflow.emit('response');
		} else {
			var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (!emailRegExp.test(req.body.email)) {
				workflow.outcome.errors.push('An email is invalid.');
				return workflow.emit('response');
			}
		}

		if (!req.body.password) {
			workflow.outcome.errors.push('A password is required.');
			return workflow.emit('response');
		}

		if (!req.body.userName) {
			workflow.outcome.errors.push('A userName is required.');
			return workflow.emit('response');
		}

		workflow.emit('duplicateUserNameCheck');
	});

	workflow.on('duplicateUserNameCheck', function() {
		req.app.db.models.Attendees.findOne({
			userName: req.body.userName
		}).exec(function(err, attendee) {
			if (err) return workflow.emit('exception', err);
			if (attendee) {
				workflow.outcome.errors.push('That userName is already taken.');
				return workflow.emit('response');
			}

			workflow.emit('duplicateEmailCheck');
		});
	});

	workflow.on('duplicateEmailCheck', function() {
		req.app.db.models.Attendees.findOne({
			email: req.body.email
		}).exec(function(err, attendee) {
			if (err) return workflow.emit('exception', err);
			if (attendee) {
				workflow.outcome.errors.push('That email is already taken.');
				return workflow.emit('response');
			}

			workflow.emit('createAttendees');
		});
	});

	workflow.on('createAttendees', function() {
		var fieldsToSet = {
			userName: req.body.userName,
			subscribeNews: req.body.subscribeNews,
			email: req.body.email,
			password: req.body.password,
			loyaltyPoints: req.body.loyaltyPoints,
			provider: req.body.provider
		};
		req.app.db.models.Attendees.create(fieldsToSet, function(err, attendee) {
			if (err) return workflow.emit('exception', err);
			workflow.outcome.record = attendee;
			return workflow.emit('response');
		});
	});

	workflow.emit('validate');
};

exports.update = function(req, res, next) {
	var workflow = new req.app.utility.workflow(req, res);
	workflow.on('validate', function() {
		if (!req.user.roles.admin.isMemberOf('root')) {
			workflow.outcome.errors.push('You may not update Attendee.');
			return workflow.emit('response');
		}
		if (!req.body.email) {
			workflow.outcome.errfor.title = 'email';
			return workflow.emit('response');
		}
		workflow.emit('patchAttendee');
	});

	workflow.on('patchAttendee', function() {
		var fieldsToSet = {
			userName: req.body.userName,
			subscribeNews: req.body.subscribeNews,
			email: req.body.email,
			password: req.body.password,
			loyaltyPoints: req.body.loyaltyPoints,
			provider: req.body.provider
		};
		req.app.db.models.Attendees.findByIdAndUpdate(req.body._id, fieldsToSet, function(err, attendee) {
			if (err) return workflow.emit('exception', err);

			return workflow.emit('response');
		});
	});

	workflow.emit('validate');
};



exports.delete = function(req, res, next) {
	var workflow = new req.app.utility.workflow(req, res);

	workflow.on('validate', function() {
		if (!req.user.roles.admin.isMemberOf('root')) {
			workflow.outcome.errors.push('You may not delete attendee.');
			return workflow.emit('response');
		}

		workflow.emit('deleteAttendee');
	});

	workflow.on('deleteAttendee', function(err) {
		req.app.db.models.Attendees.findByIdAndRemove(req.params.id, function(err, attendee) {
			if (err) return workflow.emit('exception', err);
			workflow.emit('response');
		});
	});

	workflow.emit('validate');
};