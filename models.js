'use strict';
var mongoose = require('mongoose');

exports = module.exports = function(app) {
  //embeddable docs first
  require('./schema/Note')(app, mongoose);
  require('./schema/Status')(app, mongoose);
  require('./schema/StatusLog')(app, mongoose);
  require('./schema/Category')(app, mongoose);

  //then regular docs
  require('./schema/User')(app, mongoose);
  require('./schema/Admin')(app, mongoose);
  require('./schema/AdminGroup')(app, mongoose);
  require('./schema/Account')(app, mongoose);


  // require all models. 
  var modelsPath = __dirname + '/schema/dataLayer';
  require('fs').readdirSync(modelsPath).forEach(function(file) {
    if (file.indexOf('.js') >= 0) {
      require(modelsPath + '/' + file)(app, mongoose);
    }
  });
};