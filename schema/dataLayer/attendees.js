exports = module.exports = function(app, mongoose) {
	var attendeesSchema = new mongoose.Schema({
		firstName: { type: String, default: '' },
		lastName: { type: String, default: '' },
		userName: { type: String, default: '' },
		email: { type: String, default: '' },
		password: { type: String, default: '' },
		address: { type: String, default: '' },
		provider: { type: String, default: '' },
		loyaltyPoints: { type: Number, default: 0 },
		subscribeNews: { type: Number, default: 0 }
	});
	attendeesSchema.plugin(require('../plugins/pagedFind'));
	attendeesSchema.index({ email: 1 });
	attendeesSchema.set('autoIndex', (app.get('env') == 'development'));
	app.db.model('Attendees', attendeesSchema);
}