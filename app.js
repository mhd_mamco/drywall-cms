'use strict';

//dependencies
var config = require('config'),
	express = require('express'),
	mongoStore = require('connect-mongo')(express),
	http = require('http'),
	path = require('path'),
	passport = require('passport');

//create express app
var app = express();

//setup the web server
app.server = http.createServer(app);

// * Setting up MonogDB
require('./setup/mongoose')(app, config);

// * Setting up Express server variables
require('./setup/express')(app, config, passport);

//config data models
require('./models')(app);

//setup the session store
app.sessionStore = new mongoStore({
	url: config.mongodb.uri
});

//setup passport
require('./passport')(app, passport);

//route requests
require('./routes')(app, passport);

//setup utilities
require('./utilities')(app);

//listen up
app.server.listen(app.get('port'), function() {
	//and... we're live
});