// Express Configuration.
// =====================

// Server dependencies
// -------------------
// ###NPM
// * Express.js
var express = require('express'),
	path = require('path'),
	mongoStore = require('connect-mongo')(express),

	rootPath = path.normalize(__dirname + '/..');

console.log("\nCurrent environment is: ", process.env.NODE_ENV);
console.log();

module.exports = function(app, config, passport) {
	console.log("\nRunning on port: ", config.port);
	console.log();

	//config express in all environments
	app.configure(function() {
		//settings
		app.disable('x-powered-by');
		app.set('port', config.port);
		app.set('views', rootPath + '/views');
		app.set('view engine', 'jade');
		app.set('strict routing', true);
		app.set('project-name', config.App.projectName);
		app.set('company-name', config.App.companyName);
		app.set('system-email', config.App.systemEmail);
		app.set('crypto-key', config.App.cryptoKey);
		app.set('require-account-verification', false);

		//smtp settings
		app.set('smtp-from-name', config.App.smtp.from.name);
		app.set('smtp-from-address', config.App.smtp.from.address);
		app.set('smtp-credentials', config.App.smtp.credentials);

		//twitter settings
		app.set('twitter-oauth-key', config.App.oauth.twitter.key);
		app.set('twitter-oauth-secret', config.App.oauth.twitter.secret);

		//github settings
		app.set('github-oauth-key', config.App.oauth.github.key);
		app.set('github-oauth-secret', config.App.oauth.github.secret);

		//facebook settings
		app.set('facebook-oauth-key', config.App.oauth.facebook.key);
		app.set('facebook-oauth-secret', config.App.oauth.facebook.secret);

		//middleware
		app.use(express.favicon(rootPath + '/public/favicon.ico'));
		app.use(express.logger('dev'));
		app.use(express.static(path.join(rootPath, 'public')));
		app.use(express.bodyParser());
		app.use(express.methodOverride());
		app.use(express.cookieParser());
		app.use(express.session({
			secret: config.App.cryptoKey,
			store: app.sessionStore
		}));
		app.use(passport.initialize());
		app.use(passport.session());
		app.use(app.router);

		//error handler
		app.use(require('../views/http/index').http500);

		//global locals
		app.locals.projectName = config.App.companyName;
		app.locals.copyrightYear = new Date().getFullYear();
		app.locals.copyrightName = config.App.companyName;
		app.locals.cacheBreaker = config.App.cacheBreaker;
	});

	// config express in dev environment
	// Conditionally invoke callback when env matches <process.env.NODE_ENV>
	app.configure('development', function() {
		app.use(express.errorHandler());
	});
}